<?php namespace Stocks\StockHouses;

include_once "../curl/CurlService.php";
include_once "../StockHouseException.php";

use Stocks\Curl;
use Stocks\StockHouseException;

abstract class BaseService extends Curl\CurlService {

  protected $name;
  protected $url;
  protected $marketDate;
  protected $curl;

  public function __construct() {
    if (!isset($this->url, $this->name) && !$this->isValidUrl()) {
      throw new StockHouseException(get_class($this) . " must have a $this->url and $this->name");
    }

    $this->marketDate = date('Y-m-d');

    parent::__construct();
  }

  private function isValidUrl() {
    return true;
  }

  public function getPage() { $this->run(); }

  /**
   * Get the stocks data from the returned page
   * @return mixed
   */
  public abstract function getData();

  /**
   * Convert the returned data to models
   *
   * @return mixed
   */
  public abstract function genModels();

  /**
   * Process a whole stocks scrape
   */
  public function process() {
    $this->getPage();
    $this->getData();
    $this->genModels();
  }

  public function getUrl() { return $this->url; }

  public function getName() { return $this->name; }

  public function setCurl($curl) { $this->curl = $curl; }
}
