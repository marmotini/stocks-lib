<?php namespace Stocks\StockHouses;

include_once "BaseService.php";

class NYSEService extends BaseService {

  public function __construct() {
    $this->name = 'nyse';
    $this->url =  "";

    $this->marketDate = date('');

    parent::__construct();
  }

  /**
   * Get the stocks data from the returned page
   * @return mixed
   */
  public function getData() {


  }

  /**
   * Convert the returned data to models
   *
   * @return mixed
   */
  public function genModels() {

  }
}