<?php namespace Stocks\StockHouses;

include_once "BaseService.php";

class USEService extends BaseService {

  public function __construct() {
    $this->name = 'use';
    $this->url = 'https://www.use.or.ug/market-data';

    $this->marketDate = date('');

    parent::__construct();
  }

  /**
   * Get the stocks data from the returned page
   * @return mixed
   */
  public function getData() {
    var_dump(htmlspecialchars($this->response));
  }

  /**
   * Convert the returned data to models
   *
   * @return mixed
   */
  public function genModels() {
    // TODO: Implement genModels() method.
  }
}