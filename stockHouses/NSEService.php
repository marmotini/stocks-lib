<?php namespace Stocks\StockHouses;

include_once "BaseService.php";

class NSEService extends BaseService {

  public function __construct() {
    $this->name = 'nse';
    $this->url =  "https://www.nse.co.ke/market-statistics/equity-statistics.html?view=statistics";

    $this->marketDate = date('');

    parent::__construct();
  }

  /**
   * Get the stocks data from the returned page
   * @return mixed
   */
  public function getData() {

  }
  
  /**
   * Convert the returned data to models
   *
   * @return mixed
   */
  public function genModels() {

  }
}
