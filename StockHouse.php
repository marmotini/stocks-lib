<?php namespace Stocks;

include_once "curl/CurlContext.php";
include_once "stockHouses/BaseService.php";
include_once "stockHouses/NSEService.php";
include_once "stockHouses/RSEService.php";
include_once "stockHouses/TSEService.php";
include_once "stockHouses/USEService.php";

use Stocks\Curl\CurlContext;
use Stocks\StockHouses\BaseService;
use Stocks\StockHouses\NSEService;
use Stocks\StockHouses\RSEService;
use Stocks\StockHouses\TSEService;
use Stocks\StockHouses\USEService;

class StockHouse {
  private $stockHouses = [];
  private $warnings = [];

  private $curlContext;

  public function __construct() {
    $this->stockHouses['nse'] = new NSEService();
    $this->stockHouses['rse'] = new RSEService();
    $this->stockHouses['tse'] = new TSEService();
    $this->stockHouses['use'] = new USEService();

    $this->curlContext = new CurlContext(false);
  }

  public function start($name) {
    if (isset($name)) {
      if (array_key_exists($name, $this->stockHouses)) {
        $service = $this->stockHouses[$name];

        $this->config($name, $service);

      } else {
        $this->warnings[] = new StockHouseException("Error: Service $name does not exist!");
      }
    } else {
      foreach($this->stockHouses as $name => $service) {
        $this->config($name, $service);
      }
    }

    $this->curlContext->run();
  }

  private function config($name, $service) {
    if ($service instanceof BaseService) {
      $this->curlContext->addService($service);
    } else {
      $this->warnings[] = new StockHouseException("Error: Unable to instantiate service $name");
    }
  }

  public function getWarnings() { return $this->warnings; }

}
