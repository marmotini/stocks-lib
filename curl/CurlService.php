<?php namespace Stocks\Curl;

include_once "CurlParams.php";
include_once "CurlResponse.php";
include_once "CurlError.php";

class CurlService {

  private $channel;
  protected $response;
  protected $url;
  protected $params;
  protected $error;

  public function __construct() {
    $this->params = new CurlParams();
    $this->channel = curl_init();

    $this->setUp();
  }

  public function setParamSetup(CurlParams $params) {
    $this->params = $params;

    $this->setUp();
  }

  public function setUp() {
    curl_setopt($this->channel, CURLOPT_CONNECTTIMEOUT, 0);
    curl_setopt($this->channel, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
    curl_setopt($this->channel, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($this->channel, CURLOPT_HEADER, false);
    curl_setopt($this->channel, CURLOPT_TIMEOUT, 0);
    curl_setopt($this->channel, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt($this->channel, CURLOPT_URL, $this->url);

    switch (strtoupper($this->params->getMethod())) {
      case 'POST':
        $this->postify();
        break;
      case 'GET':
      default:
        $queryData = $this->getify();
        if (!is_null($queryData)) {
          curl_setopt($this->channel, CURLOPT_URL, $queryData);
        }
    }
  }

  public function run() {
    $content = curl_exec($this->channel);
    $status = curl_getinfo($this->channel, CURLINFO_HTTP_CODE);
    $downloadSpeed = curl_getinfo($this->channel, CURLINFO_SPEED_DOWNLOAD);
    $uploadSpeed = curl_getinfo($this->channel, CURLINFO_SPEED_UPLOAD);

    $this->error = new CurlError(curl_error($this->channel), curl_errno($this->channel));
    $this->response = new CurlResponse($status, $content, $downloadSpeed, $uploadSpeed);

    return $this->response;
  }

  public function getChannel() { return $this->channel; }

  private function postify() {
    curl_setopt($this->channel, CURLOPT_POST, 1); // setting as a post
    curl_setopt($this->channel, CURLOPT_POST, count($this->params->getData()));

    $queryData = $this->querifyData();

    if (!empty($queryData)) {
      curl_setopt($this->channel, CURLOPT_POSTFIELDS, $queryData);
    }
  }

  private function querifyData() {
    $vars = '';

    foreach($this->data as $key=>$value) {
      $vars .= urlencode($key) . '=' . urlencode(is_array($value) ? json_encode($value, JSON_UNESCAPED_SLASHES) : $value) . '&';
    }

    return rtrim($vars, '&');
  }

  private function getify() {
    $queryData = $this->querifyData();

    return (!empty($queryData)) ?$this->url . '?' . $queryData : null;
  }

}
