<?php namespace Stocks\Curl;

class CurlError {
  private $error;
  private $errorNo;

  public function __function($error, $errorNo) {
    $this->error = $error;
    $this->errorNo = $errorNo;
  }

  public function getError() { return $this->error; }
  public function getErrorNo() { return $this->errorNo; }
}