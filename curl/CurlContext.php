<?php namespace Stocks\Curl;

include_once "../stockHouses/BaseService.php";
include_once "CurlService.php";
include_once "CurlParams.php";

use Stocks\StockHouses\BaseService;

class CurlContext {

  private $services = [];
  private $multiHandler;
  private $responses = [];
  private $processMulti = false;

  public function __construct($processMulti) {
    $this->processMulti = $processMulti;
  }

  public function addService(BaseService $service) {
    $service->setParamSetup(new CurlParams());

    $this->services[$service->getName()] = $service;
  }

  public function run() {
    if ($this->processMulti) {
      $this->runMultiCurls();
    } else {
      $this->runSingleCurls();
    }
  }

  private function genMultiCurls() {
    $this->multiHandler = curl_multi_init();

    foreach($this->services as $name => $service) {
      curl_multi_add_handle($this->multiHandler, $service->getChannel());
    }
  }

  public function runSingleCurls() {
    foreach($this->services as $name => $service) {
      $this->responses[$name] = $service->process();

      curl_close($service->getChannel());
    }
  }

  public function runMultiCurls() {
    $this->genMultiCurls();

    $running = null;

    if (isset($this->multiHandler)) {
      do {
        $status = curl_multi_exec($this->multiHandler, $running);
      } while ($running > 0 || $status === CURLM_CALL_MULTI_PERFORM);
    }

    foreach($this->services as $name => $service) {
      $this->responses[$name] = $this->getMultiChannelResponse($service->getChannel());

      curl_close($service->getChannel());
      curl_multi_remove_handle($this->multiHandler, $service->getChannel());
    }
  }

  private function getMultiChannelResponse(BaseService $curl) {
    $content = curl_multi_getcontent($curl->getChannel()); // get the content
    $url = curl_getinfo($curl->getChannel(), CURLINFO_EFFECTIVE_URL);
    $status = curl_getinfo($curl->getChannel(), CURLINFO_HTTP_CODE);
    $curlResponse = new CurlResponse($status, $content, $url);

    return $curlResponse;
  }

  public function getResponses() { return $this->responses; }

}
