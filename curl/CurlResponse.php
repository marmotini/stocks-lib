<?php namespace Stocks\Curl;

class CurlResponse {

  private $httpStatus;
  private $content;
  private $downloadSpeed;
  private $uploadSpeed;

  public function __construct($httpStatus, $content, $downloadSpeed, $uploadSpeed) {
    $this->httpStatus = $httpStatus;
    $this->content = $content;
    $this->downloadSpeed = $downloadSpeed;
    $this->uploadSpeed = $uploadSpeed;
  }

  public function getContent() { return $this->content; }
  public function getStatusCode() { return $this->httpStatus; }
  public function getUploadSpeed() { return $this->uploadSpeed; }
  public function getDownloadSpeed() { return $this->downloadSpeed; }

  public function setContent($content) { $this->content = $content; }
  public function setDownloadSpeed($downloadSpeed) { $this->downloadSpeed = $downloadSpeed; }
  public function setUploadSpeed($uploadSpeed) { $this->uploadSpeed = $uploadSpeed; }
  public function setStatusCode($status) { $this->httpStatus = $status; }

}