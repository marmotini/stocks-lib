<?php namespace Stocks\Curl;

class CurlParams {

  private $method = 'get';
  private $data = [];

  public function setMethod($method) { $this->method = $method; }
  public function setData($data) { $this->data = $data; }

  public function getMethod() { return $this->method; }
  public function getData() { return $this->data; }

}